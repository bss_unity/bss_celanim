﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace BSS.CelAnim {
#if ODIN_INSPECTOR
    using Sirenix.OdinInspector;
#endif

    /// <summary>
    /// Spirte Sequence 애니메이션 파일
    /// </summary>
    [CreateAssetMenu(fileName = "NewCelAnimation", menuName = "BSS/Cel Animation/Cel Animation Asset", order = 100)]
    public class CelAnimation : ScriptableObject {
#if ODIN_INSPECTOR
        [ShowInInspector]
#endif
        public float realDuration => animFrames.Count == 0 ? 0f : GetRealDuration();


#if ODIN_INSPECTOR
        [ReadOnly]
#endif
        public List<AnimationFrame> animFrames = new List<AnimationFrame>();

        [SerializeField]
        public int fps = 30;
        //Property
        public int FPS { get { return fps; } set { fps = value; } }
        public int FramesCount { get { return animFrames.Count; } }


        private float GetRealDuration() {
            int frameSum = animFrames.Sum(x => x.duration);
            return ((float)frameSum / (float)FPS);
        }

#if ODIN_INSPECTOR
        [BoxGroup("Edit Mode")]
        [SerializeField]
        private List<Sprite> editFrames = new List<Sprite>();
        [BoxGroup("Edit Mode")]
        [Button(ButtonSizes.Medium)]
        private void Apply() {
            if (editFrames.Count == 0) return;
            editFrames.Sort((x,y) => {
                return x.name.CompareTo(y.name);
            });
            animFrames.Clear();
            for (int i = 0; i < editFrames.Count; i++) {
                animFrames.Add(new AnimationFrame(editFrames[i], 1));
            }
            editFrames.Clear();
        }
        [BoxGroup("Edit Mode")]
        [Button(ButtonSizes.Medium)]

        private void Clear() {
            animFrames.Clear();
        }
#endif
    }
}