﻿using UnityEngine;


namespace BSS.CelAnim {
    /// <summary>
    /// 스프라이트 렌더러 애니메이터
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    [DisallowMultipleComponent]
    public class SpriteCelAnimator : BaseCelAnimator
    {
        protected override void ChangeFrame(Sprite frame)
        {
            GetComponent<SpriteRenderer>().sprite = frame;
        }
    }
}