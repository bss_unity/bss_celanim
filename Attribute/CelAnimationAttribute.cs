﻿using UnityEngine;
using System;

namespace BSS.CelAnim {
	[AttributeUsage(AttributeTargets.Field)]
	public class CelAnimationFieldAttribute : PropertyAttribute { }
}